---
author: "Benjamin Graham | Jason Zweig | Warren E. Buffett "
date: 2021-03-10
title: "The Intelligent Investor: The Definitive Book on Value Investing. A Book of Practical Counsel (Revised Edition)"
shortTitle: "The Intelligent Investor"
cover: "http://books.google.com/books/content?id=nJGoCgAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
amazon: "https://www.amazon.com/Intelligent-Investor-Definitive-Investing-Essentials/dp/0060555661"
tag: "personal finance"
---


The Intelligent Investor describes the investing philosophy and strategy of Benjamin Graham. 
How to find great companies, and tell if that company will make a good investment.

  <!--more-->

# There are two approaches to investing:

1. **Active/Enterprising** - Researching and picking your own stocks. It takes lots of time and energy.
2. **Passive/Defensive** - Pick an index fund, setup monthly contribution, and never touch it or check it again. Requires no effort, but less excitement, and possibly less return. Surprisingly hard to resist checking it and self sabotaging.  

Both approaches work well if you're honest with yourself and pick the right one for you. Don't do active investing if you're not going to put in the time and work, or if you'll get caught up in news and panic through market cycles. Feel free to use a combination, most people do, with a percentage split that applies to your personality.

This book divides techniques between the aggressive and passive approach. The passive approach boils down to "just invest in index funds". You can read my summary on [Boggleheads Guide to investing](https://bookbreakdowns.com/post/boggleheads-guide-to-investing/) for the same advice. The rest of my summary will focus on the active approach to investing, which covers new content for me.

# How to Find Companies to Invest In

## The Relatively Unpopular Large Company

Focus on large companies going through temporary unpopularity. You'll see plenty of undervalued smaller companies that have more potential to grow, but larger companies have a couple of advantages:

1. Larger companies have the capital, skill, resources, track record to have a better chance to bounce back from adversity
2. The market is more likely to respond quickly to improvement in a well known company with a good track record

Look at daily list of 52 week lows for stocks and industries that are less trendy. For example: In July 2002, Johnson & Johnson announced a federal investigation into their record keeping, causing their stock price to drop from P/E of 24 to 20.

## Middle Sized (Secondary) Companies

In Bull markets, secondary companies (closest competitor to industry leader) are big enough to weather potential storms, but offer higher growth potential than the industry leader in bull markets. However, secondary companies have a higher chance to fall, and fall farther during bear markets. 

Most investors overvalue secondary companies in bull markets when their stock prices are outperforming larger companies. But then undervalue these companies after the experience of getting burned during the bear market. Both of these are over exaggerated positions to have. Middle sized companies are still large when compared to most privately held companies, with no reason to not expect them to bounce back.

## Buy What you Know

[Peter Lynch](https://en.wikipedia.org/wiki/Peter_Lynch) argued that amateur investors have an advantage over professionals: “the power of common knowledge.” If you discover a great product or service, then you have a personal insight into a stock that a professional might never pick up on.

> You can outperform the experts if you use your edge by investing in companies or industries you already understand. - Peter Lynch

# How to Analyze Whether a Company is a Good Company

## Read the 10K Annual Report

Start with the annual 10K report from EDGAR database at www.sec.gov. Or just google the company you're looking into with "investor relations". Try to answer these questions:

### What makes the company grow? Where does its profits come from?
- Does the company have a moat/competitive advantage - strong brand, monopoly on market, economies of scale, unique intangible asset, resistance to substitution.
- Acquisitions? 2-3 acquisitions a year is a sign of potential trouble. If the company would rather buy stock of other businesses than invest in it's own, you should too.
- Borrowing debt or selling stock (cash from financing activities on cash flow statement).
- Relying on a single or few customer for bulk of revenue.

### Does the company have a good management team?
- Do they fulfill promises from previous 10K reports? Do they take responsibility or blame it on other factors?
- What is CEO pay?
- Are they repricing or reissuing or exchanging stock options for insiders?
- Are the company's financial statements easily understandable? or full of obfuscation?
- Form 4 shows whether a firms senior executives have been buying or selling share. Repeated selling is a red flag
- Do they have transparent accounting practices? Constant "nonrecurring" or "extraordinary" items are bad. Acronyms like EBITDA over net income, or "pro forma" earnings are often used to cloak actual losses

### Is the company investing into its future? 
- About 4-10% of net sales on R&D depending on industry.
- Return on Invested Capital (ROIC) - ROIC represents what the company earns from its operating businesses, and how efficiently it uses shareholders money to generate that return. ROIC of at least 10% is attractive. 6%-7% can be tempting if the company has good brand names, focused management, or is under a temporary cloud.

### What is the financial strength of the company?
- Do they generate more cash than they consume
- Is its revenues/net earnings growing smoothly. 10% is sustainable. over 15% or sudden burst will fade.
- Owner earnings growing at 6% per year over the last 10 years is good
- 2:1 ratio  current assets to current liabilities. long-term debt should not exceed net current assets (working capital).  Current assets at least 1.5 times current liabilities, and debt not more than 110% of net current assets

This is Warren's Way. He has set out his thinking like an open book in his annual reports archived at [www.berkshirehathaway.com](www.berkshirehathaway.com)

## Read Financial Statements

You must always follow these two rules when looking at a companies earnings.

1. Don't take any single year's earnings seriously
2. If you do pay attention to short-term earnings, look out for booby traps in the per-share figures

Why? Because it's easy to manipulate company's earnings numbers year to year, hiding expenses or creating earnings. ALWAYS read the footnotes to the financial statements in the annual report. Be sure to compare the footnotes with those in the financial statements of at least one firm that's a close competitor, to see how aggressive your companies accountants are. 

### Things to look out for:

- **Recurring "non recurring" costs** - For example, mergers, restructuring, charges for impairment of long lived assets, write-offs of purchased in-process R&D
- **Pro Forma Earnings** - Show what a company would have earned over the past year if another firm it just acquired had been part of the family for the entire 12 months. Or earnings if it had not paid dividends, or not payed payroll taxes on stock options exercised by its employees, or not lost in investing in lousy stocks. basically anything they wave off as an irregularity
- **Aggressive Revenue Recognition** - Often a sign of danger. Qwest in 1999 changed accounting principle to recognize revenue from telephone directories as soon as they were published, before receiving any actual revenue. Pumped up net income by $240M.
- **Capital Expenditure** - Global Crossing spent > $600M on construction which would have turned reported net income from -$96M to $82 in profit. Next year moved those costs from operating costs to capital expenditure, increasing company's total assets, instead of decreasing its net income.

Using average earnings over 5-10 years is an easy way to smooth out these special charges, since these should be a part of the company's operating history, and will show up in average earnings.

### More resources for how to read financial statements:
- Financial Statement Analysis - Martin Fridson and Fernando Alvarez
- The financial numbers Game - Charles Mulford and Eugene Comiskey
- Financial Shenanigans - Howard Shilit

# How to Analyze Whether a Good Company is a Good Investment

> The value of any investment is, and always must be, a function of the price you pay for it. 

Finding a promising company is only the first step. Remember that even great companies (maybe even especially) can still be over valued, which can make a great company a bad investment. You still need to Compare the current price to its fair value. 

Think of it this way: The Chicago Bulls got a bargain by paying Michael Jordan up to $34 million a year. But does that mean it was worth $340 million, or $3.4 billion, or $34 billion, per season? At some point even the best company is overpriced. 

## There are two ways you can be wrong about an investment

1. You can be wrong about whether a company is a good company
2. You can be right about whether a company is good, but wrong about what it's worth.

You can tell if a company is undervalued by estimating future earnings and comparing it to the current price. The current price should not be more than 15 times average earnings of the past three years. This means that even if a company's price stock drops, it may still be over valued. Wall Street uses Forward P/E ratios. Which compares current price to predicted future earnings. There are pros and cons to both methods.

## A Two Part Appraisal Process

1. First calculate what the stock is worth based assuming past performance will continue into the future
2. Then do a second analysis based on adjustments you predict in the future (new demand, expanded products, market changes etc)

Compare the two analysis, with notes explaining any adjustments made in the second. Your predictions will probably be wrong, but keep doing this so you can build up a record. You can use this a good measuring stick when doing future analysis. For example you'll have get a pretty good idea of what range of outcomes to expect based on your analysis, and can adjust expectations going forward.

## Due Diligence

Check out the neighborhood. websites like quicktake.morningstar.com, finance.yahoo.com and quicken.com can tell you what percentage of a company's shares are owned by institutions. Anything over 60% suggests that a stock is scarcely undiscovered and probably over-owned. When big institutions sell, they tend to move in lockstep, with disastrous results for the stock. Those websites will also tell you who the largest owners of the stock are. If they are money management firms that invest in a style similar to your own, that's a good sign.

## "Margin of Safety" as the Central Concept of Investment

Use a margin of safety by only investing in a company when the price of the stock is less than two thirds of what you calculated as fair value. By refusing to pay too much for an investment, you minimize the chances that your wealth will ever disappear or suddenly be destroyed. Losing 50% in one year will need 10% gain every year for 18 years to catch back up to market at 5% per year.

# Case Studies

## Aluminum of America (ALCOA) - Accounting Tricks

ALCOA had EPS of $5.20 in 1970, with it's stock selling at $62, just 10x earnings. But with a footnote explaining this is "primary earnings" before special charges. After special charges, earnings was just 2.80,  P/E ratio of 22.

In this case, footnote explains the $18M special charges as an estimate of anticipated costs of closing down manufactured products division, phasing out ALCOA Credit Co. and completion of the contract for a "curtain wall.". In other words, not considered part of "normal operations"

Anticipating future losses allows the company to hide it from a specific year. They don't belong in 1970 because they were not actually taken in that year, and won't be shown in the year they are actually taken because they have already been provided for.

If ALCOA figure represents future losses before the related tax credit, then not only will future earnings be freed from the weight of these charges when they're actually incurred, but they will be INCREASED by a tax credit of some 50% thereof. Tax credits resulting from past years losses are now being shown separately as "special items", but they will enter into future statistics as part of the final "net income" figure. 

Everyone expected poor results during the 1970 bear market for most companies, but anticipating better results going forward. How nice to charge as much as possible to the bad year, clearing the way for some nice numbers in the next few years!

Use of Average Earnings - average earnings over say 7-10 years is useful for ironing out these accounting tricks for a specific year. It solves the problem of what to do with special charges and credits, since they will be part of the company's operating history and will show up in average earnings.

## AOL - Revenue Sources

AOL had $10.3B in assets with revenue of $5.7B. Time Warner had $51B in assets and revenue of $27.3B. Time Warner was bigger by any measure except one: the valuation of its stock. Because AOL bedazzled investors simply by being in the internet industry, its stock sold for a stupendous 164 times its earnings. Stock in Time Warner sold for around 50 times earnings.

Nearly half of AOLs total assets was made up of "available-for-sale equity securities". If the prices of publicly traded technology stocks fell, that could wipe out much of the company's asset base. In 2001 merger finalized. AOL Time Warner lost $4.9B, then $98.7B the next year. Most of the losses came from writing down the value of AOL

## CISCO - Mergers and Acquisitions

On March 27, 200 Cisco Systems, Inc became the world's most valuable corporation as its stock hit $548B in total value. Over 10 years since it's IPO the stock had risen 103,697%, 217% per year. Currently at 14.9B in revenue and 2.5B in earnings. The stock was trading at 219 times net income.

But much of Cisco's growth in revenue and earnings came from acquisitions. Since September alone, Cisco had ponied up $10.2B to buy 11 other firms.  and 1/3 of Cisco's earnings over the previous six months came not from its businesses, but from tax breaks on stock options exercised by its executives and employees. And Cisco had gained $5.8B selling "investments", then bought $6B more. Was it an internet company or a mutual fund?

No company as big as Cisco had ever been able to grow fast enough to justify a P/E ratio above 60 - let alone over 200. Once a company becomes a giant, its growth must slow down. A company can be a giant, or it can deserve a giant P/E ratio, but both together are impossible.

## Ball and Stryker - Market Panics

Between July 9 and July 23, 2002 Ball Corp.s stock dropped from $43.69 to $33.48 - a loss of 24%. Over the same two weeks, Stryker Corp's shares fell from $49.55 to $45.60, an 8% drop. 

Stryker, issued only one press release during the two weeks. On July 16, Stryker announced that its sales grew 15% to $734M in the second quarter, while earnings jumped 31% to $86M. Ball issued no press releases at all during those two weeks. On July 25, however, Ball reported that it had earned $50M on sales of $1B in the second quarter, a 61% rise in net income over the same period one year earlier.

What, then, had pounded these two stocks down? Between July 9 and July 23, 2002, the Dow Jones Industrial Average fell from 9096 to 7702, a 15% plunge. The good news at Ball and Stryker got lost in the bad headlines and falling markets, which took these two stocks down with them.

The intelligent investor should recognize that market panics can create great prices for good companies.

# What Does NOT Work

## Don't Formula Trade

**Formula investment plans -** A formula to automatically buy or sell when the market reaches certain benchmarks. For example [The Foolish Four]() or [The January Effect]().

This approach has the double appeal of sounding logical (and conservative) and of showing excellent results when applied retrospectively to the stock market over the past. But don't believe it:

- You can find all sorts of patterns in stock market data if you have enough of it. But most of them will be by chance, and probably won't continue over the next 10 years.
- Even if it is a valid pattern, eventually enough people will join in, changing market until the old formula no longer fits.
- The passage of time brings new conditions, until the old formula no longer fits. 

Formula's and other mechanical investing steps may work temporarily, but that doesn't mean it works. If you drive to work at 130 MPH and save an hour from your day, did you discover something that works? Or will your luck eventually end?  

## Don't Time The Market

The most realistic distinction between the investor and the speculator is found in their attitude toward stock-market movements. 
- The speculator’s primary interest lies in anticipating and profiting from market fluctuations. Buy a stock before it's moved trying to guess which way it's going to go.
- The investor’s primary interest lies in acquiring and holding great companies at great prices. Buy a stock after it's moved to a price that makes it a good investment.

[Robert Shiller](https://en.wikipedia.org/wiki/Robert_J._Shiller) compared historical P/E ratio of the S&P 500. A P/E ratio above 30 means we're heading back down, a dip below 10 usually correlates with an upward trend in the near future.

## Don't Get Caught Up in the Hype

All investors know to buy low and sell high. But most investors get this wrong. When the stock market is on a bull run for multiple years, media and expect it to continue and act accordingly. When the stock market is on a bear run for multiple years, analysts always do the opposite. For example:

- from 1995 - 1999, market rose at least 20% per year. Buyers became more optimistic. Analysts, company pension plans, everyone was projecting 10%/year growth going forward
- In 2001 prices were about 50% lower than the previous year. Those same experts were now projecting 7%/year return and lost $32B from pension plans. 

They bought too much by being overly enthusiastic when the market was "high", and sold at the bottom when things were looking bad. Just buy and hold.

## Don't Day Trade

When you trade instead of invest, you turn long-term gains (taxed at a maximum capital-gains rate of 20%) into ordinary income (taxed at a maximum rate of 38.6%). You also lose money to “market impact”.

## Don't buy IPOs 

For every IPO like Microsoft that turns out to be a big winner, there are thousands of losers. Big investment banks and funds houses get shares at the initial (or “underwriting”) price, before the stock begins public trading. If, like nearly every investor, you can get access to IPOs only after their shares have rocketed above the exclusive initial price, your results will be terrible. From 1980 through 2001, if you had bought the average IPO at its first public closing price and held on for three years, you would have underperformed the market by more than 23 percentage points annually.

# Last Words Before You Start

1. **Separate your funds -** Keep most of your investments in an account with index funds. Keep no more than 10% of your portfolio in a separate "fun money" account for investing into individual stocks. Never mix these accounts! Gambling is part of human nature, the only way to be sure it doesn't take over and  take away more than you're willing to lose is to lock it away in a separate account.
2. **Practice -** Take a year to practice picking stocks. track your progress with a portfolio tracker. Then compare your theoretical years performance against s&p 500. If you don't like it, you can stop wasting time
3. **Understand your own psychology -** The biggest risk isn't in the stocks you pick, it's in your own psychology. The market will always go up and down, will you panic as it goes through these cycles. These questions might help you answer that for yourself:
	1. Do you have enough cash to support yourself for a year? So you don't panic sell as soon as things go bad?
	2. Have you survived a previous market crash? How did you and your portfolio react. Don't put too much money in until you've lived through the real thing.

# Your Advantages

Here are some of the handicaps mutual-fund managers and other professional investors are saddled with:

- With billions of dollars under management, they are limited to the largest stocks—ones they can buy in the multimillion-dollar quantities they need to fill their portfolios. Thus many funds end up owning the same few overpriced giants.
- If fund investors ask for their money back when the market drops, the managers may need to sell stocks to cash them out. Just as the funds are forced to buy stocks at inflated prices in a rising market, they become forced sellers as stocks get cheap again.
- If a company gets added to an index, hundreds of funds compulsively buy it. (If they don’t, and that stock then does well, the managers look foolish; on the other hand, if they buy it and it does poorly, no one will blame them.)
- Fund managers are expected to specialize, and are limited to only stocks matching their funds "specialty", for example only “small growth” stocks, or only “mid-sized value”.


