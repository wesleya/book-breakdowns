# Book Breakdowns

## Book Covers

* https://www.googleapis.com/books/v1/volumes?q=the+book+title
* https://www.googleapis.com/books/v1/volumes?q=isbn:1455509124
* http://books.google.com/books/content?id=7kXHkQEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api

## Affiliate Link

* https://affiliate-program.amazon.com/home
* enter ISBN in the search box
* click link to copy affiliate link for the product. make sure not to copy the link for the search!

## Dev Setup

1. Install [Hugo](https://gohugo.io/)
2. Clone repo: `git clone git@gitlab.com:wesleya/book-breakdowns.git`
4. Run hugo server `hugo server -D`

## Production Setup

1. hosted on gitlab pages
2. hover domain using cloudflare nameservers
3. cloudflare also handles the ssl cert and redirect to https

## GitLab Pages Pipeline

1. gitlab.com -> project -> CI/CD -> pipelines -> run pipeline